from django import forms
from django.forms import ModelForm

from Comercial.models import Cuota
from PE.validators import no_future_date_validator


class CuotaUpdateForm(ModelForm):
    fecha_pago = forms.DateField(validators=[no_future_date_validator])
    class Meta:
        model = Cuota
        fields = ['fecha_pago']