import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.timezone import now

from Entreno.models import Atleta


class LegajoComercial(models.Model):
    TALLES_CHOICES = ((1, 'XXS'),
                      (2, 'XS'),
                      (3, 'S'),
                      (4, 'M'),
                      (5, 'L'),
                      (6, 'XL'),
                      (7, 'XXL'))

    atleta = models.OneToOneField(Atleta, on_delete=models.CASCADE)
    fecha_inscripcion = models.DateField(default=now)
    activo = models.BooleanField(default=True)
    becado = models.BooleanField(default=False)

    talle = models.IntegerField(choices=TALLES_CHOICES, null=True, blank=True)

    def get_absolute_url(self):
        return reverse('Comercial:legajos:detail', kwargs={'pk': self.id})

    @property
    def con_deuda(self):
        return self.activo and not self.becado and self.cuota_set.filter(fecha_pago__isnull=True).exists()

    class Meta:
        ordering = ["atleta__apellido", "atleta__nombre"]
        verbose_name = "Legajo Comercial"
        verbose_name_plural = "Legajos Comerciales"

    def __str__(self):
        return 'Leg. ' + self.atleta.apellido + ', ' + self.atleta.nombre


def anio_actual():
    return now().year


class Periodo(models.Model):
    MESES_CHOICES = ((1, "Enero"),
                     (2, "Febrero"),
                     (3, "Marzo"),
                     (4, "Abril"),
                     (5, "Mayo"),
                     (6, "Junio"),
                     (7, "Julio"),
                     (8, "Agosto"),
                     (9, "Septiembre"),
                     (10, "Octubre"),
                     (11, "Noviembre"),
                     (12, "Diciembre"))
    fecha = models.DateField(default=now)
    mes = models.IntegerField(choices=MESES_CHOICES)
    anio = models.IntegerField(default=anio_actual)
    debitado = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Periodo"
        verbose_name_plural = "Periodos"
        unique_together = ('anio', 'mes')

    def __str__(self):
        return str(self.get_mes_display()) + '/' + str(self.anio)


class Cuota(models.Model):
    fecha = models.DateField(default=now)
    legajo = models.ForeignKey(LegajoComercial, on_delete=models.CASCADE)
    periodo = models.ForeignKey(Periodo, on_delete=models.CASCADE)
    fecha_pago = models.DateField(null=True, blank=True)

    def get_absolute_url(self):
        return reverse('Comercial:cuotas:update', kwargs={'pk': self.id})

    class Meta:
        verbose_name = "Cuota"
        verbose_name_plural = "Cuotas"

    def __str__(self):
        return str(self.id) + ' ' + str(self.periodo)


@receiver(post_save, sender=Cuota)
def __pasar_inactivo_legajo_post_cuota_save(sender, **kwargs):
    try:
        cuota = kwargs.get('instance')
        legajo = cuota.legajo
        conteo = Cuota.objects.filter(legajo=legajo, fecha_pago__isnull=True).count()
        legajo.activo = 2 > conteo
        legajo.save()

    except Exception as e:
        raise e


@receiver(post_save, sender=Periodo)
def __debitar_cuotas_por_periodo_post_save(sender, **kwargs):
    periodo = kwargs.get('instance')  # type: Periodo
    if not periodo.debitado:
        legajos = LegajoComercial.objects.exclude(activo=False, becado=True)
        for legajo in legajos:
            Cuota(legajo=legajo, periodo=periodo).save()
        periodo.debitado = True
        periodo.save()


@receiver(post_save, sender=Atleta)
def __crear_legajo_comercial_on_atleta_post_save(sender, **kwargs):
    try:
        atleta = kwargs.get('instance')  # type: Atleta
        legajo, created = LegajoComercial.objects.get_or_create(atleta=atleta)
        logging.info(f'Legajo comercial: {legajo} Creado: {created}')
    except Exception as e:
        logging.exception(e)
        raise e
