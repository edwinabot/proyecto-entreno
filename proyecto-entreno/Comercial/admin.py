from django.contrib import admin

from . import models

admin.site.register(models.LegajoComercial)
admin.site.register(models.Cuota)
admin.site.register(models.Periodo)