from django import forms
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from .models import LegajoComercial, Cuota


class LegajosTodosList(ListView):
    template_name = 'legajos_comerciales_list.html'
    queryset = LegajoComercial.objects.all()


class LegajosDeudoresList(ListView):
    template_name = 'LegajosDeudores_list.html'
    model = LegajoComercial

    def get_context_data(self, **kwargs):
        context = super(LegajosDeudoresList, self).get_context_data(**kwargs)
        legs = LegajoComercial.objects.all()
        deudores = []

        for l in legs:
            if l.activo and not l.becado and l.cuota_set \
                    .filter(fecha_pago__isnull=True).exists():
                deudores.append(l)

        context['object_list'] = deudores
        return context


class LegajosNoDeudoresList(ListView):
    template_name = 'LegajosNoDeudores_list.html'
    model = LegajoComercial

    def get_context_data(self, **kwargs):
        context = super(LegajosNoDeudoresList, self).get_context_data(**kwargs)
        legs = LegajoComercial.objects.all()
        no_deudores = []

        for l in legs:
            if l.activo and (l.becado or not l.cuota_set
                    .filter(fecha_pago__isnull=True).exists()):
                no_deudores.append(l)

        context['object_list'] = no_deudores
        return context


class LegajosInactivosList(ListView):
    template_name = 'LegajosInactivos_list.html'
    queryset = LegajoComercial.objects \
        .filter(activo=False)


class CuotasDelLegajo(TemplateView):
    template_name = 'CuotasDelLegajo.html'

    class CuotasDelLegajoForm(forms.Form):
        legajo = forms.ModelChoiceField(queryset=LegajoComercial.objects.all())

    def get_context_data(self, **kwargs):
        context = super(CuotasDelLegajo, self).get_context_data(**kwargs)
        context['form_cuotas'] = self.CuotasDelLegajoForm()

        if 'legajo' in self.request.GET:
            leg = LegajoComercial.objects.get(id=self.request.GET['legajo'])
            context['legajo'] = leg
            context['object_list'] = Cuota.objects.filter(legajo=leg)

        return context
