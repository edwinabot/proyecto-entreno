import logging
import threading
from datetime import datetime, timedelta

from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

from Comercial.forms import CuotaUpdateForm
from .models import LegajoComercial, Cuota, Periodo

FECHA_TESTIGO = datetime.now()  # type: datetime


def debitar_periodo(ahora):
    ultimo_periodo = Periodo.objects.order_by('-id')[0]
    if ultimo_periodo.anio < ahora.year or ultimo_periodo.mes < ahora.month:
        nuevo_periodo = Periodo(mes=ahora.month, anio=ahora.year, fecha=ahora)
        nuevo_periodo.save()
        return nuevo_periodo


def check_thread(thread_event, logger):
    global FECHA_TESTIGO
    ahora = datetime.now()
    if FECHA_TESTIGO.year < ahora.year or FECHA_TESTIGO.month < ahora.month:
        periodo = debitar_periodo(ahora)
        FECHA_TESTIGO = ahora
        logger.info(f'Debitado el periodo: {periodo}')
    if not thread_event.is_set():
        maniana = ahora + timedelta(days=1)
        sleep_time = maniana - ahora
        thread = threading.Timer(sleep_time.total_seconds(), check_thread, [thread_event, logger])
        thread.start()


def start_monitor():
    logger = logging.getLogger(__name__)
    logger.setLevel('DEBUG')
    event = threading.Event()
    logger.info("Iniciando thread de debito de periodos")
    check_thread(event, logger)


class CuotaDetail(DetailView):
    template_name = 'cuota_detail.html'
    model = Cuota


class CuotaUpdate(LoginRequiredMixin, UpdateView):
    template_name = 'cuota_form.html'
    model = Cuota
    form_class = CuotaUpdateForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CuotaUpdate, self).get_context_data(**kwargs)
        # Add in context data
        cuota = context['object']
        context['titulo'] = "Actualizando una cuota"
        context['subtitle'] = f'{cuota.periodo}. {cuota.legajo.atleta}'
        return context


class CuotasImpagas(LoginRequiredMixin, ListView):
    template_name = 'cuota_list.html'
    model = Cuota
    queryset = Cuota.objects.filter(fecha_pago__isnull=True).all()
    success_url = reverse_lazy('Comercial:cuotas:impagas')


class CuotasDelLegajo(LoginRequiredMixin, TemplateView):
    template_name = 'CuotasDelLegajo.html'

    class CuotasDelLegajoForm(forms.Form):
        legajo = forms.ModelChoiceField(queryset=LegajoComercial.objects.all())

    def get_context_data(self, **kwargs):
        context = super(CuotasDelLegajo, self).get_context_data(**kwargs)
        context['form_cuotas'] = self.CuotasDelLegajoForm()

        if 'legajo' in self.request.GET:
            leg = LegajoComercial.objects.get(id=self.request.GET['legajo'])
            context['legajo'] = leg
            context['object_list'] = Cuota.objects.filter(legajo=leg)

        return context


class LegajoUpdate(LoginRequiredMixin, UpdateView):
    model = LegajoComercial
    fields = ['fecha_inscripcion', 'activo', 'becado', 'talle']
    template_name = 'legajo_form.html'
    success_url = reverse_lazy('Comercial:legajos:list')

    def get_context_data(self, **kwargs):
        context = super(LegajoUpdate, self).get_context_data(**kwargs)
        legajo = context['object']  # type: LegajoComercial
        context['titulo'] = f"Legajo comercial: {legajo.atleta}"
        return context


class LegajoComercialDetail(DetailView):
    model = LegajoComercial
    template_name = 'legajocomercial_detail.html'

    def get_context_data(self, **kwargs):
        context = super(LegajoComercialDetail, self).get_context_data(**kwargs)
        context['titulo'] = f"Legajo comercial"
        return context


class LegajosTodosList(ListView):
    template_name = 'legajo_list.html'
    queryset = LegajoComercial.objects.all()
