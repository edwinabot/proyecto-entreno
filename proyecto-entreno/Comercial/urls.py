from django.urls import path, include
from . import views

urls_cuotas = [
    path('<pk>/update', views.CuotaUpdate.as_view(), name='update'),
    path('impagas', views.CuotasImpagas.as_view(), name='impagas')
]

urls_legajo = [
    path('list', views.LegajosTodosList.as_view(), name='list'),
    path('<pk>/update', views.LegajoUpdate.as_view(), name='update'),
    path('<pk>/detail', views.LegajoComercialDetail.as_view(), name='detail')
]

urlpatterns = [
    path('cuotas/', include((urls_cuotas, 'Comercial'), namespace='cuotas')),
    path('legajos/', include((urls_legajo, 'Comercial'), namespace='legajos'))
]