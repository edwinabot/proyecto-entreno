# Generated by Django 2.0 on 2018-01-05 16:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Comercial', '0002_auto_20171231_0351'),
    ]

    operations = [
        migrations.RenameField(
            model_name='legajocomercial',
            old_name='talle_remera',
            new_name='talle',
        ),
        migrations.RemoveField(
            model_name='legajocomercial',
            name='formas_contacto',
        ),
    ]
