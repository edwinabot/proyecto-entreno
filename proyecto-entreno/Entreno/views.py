from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, DetailView, UpdateView, ListView

from Entreno.forms import AtletaForm
from Entreno.models import Atleta
from . import models


class AtletaDetail(LoginRequiredMixin, DetailView):
    template_name = 'atleta_detail.html'
    model = models.Atleta

    def get_context_data(self, **kwargs):
        context = super(AtletaDetail, self).get_context_data(**kwargs)
        atleta = context['object']  # type: models.Atleta
        context['titulo'] = f"{atleta}"
        return context


class AtletaCreate(LoginRequiredMixin, CreateView):
    template_name = 'atleta_form.html'
    model = models.Atleta
    form_class = AtletaForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AtletaCreate, self).get_context_data(**kwargs)
        # Add in context data
        context['titulo'] = "Cargando un atleta"
        return context


class AtletaUpdate(LoginRequiredMixin, UpdateView):
    template_name = 'atleta_form.html'
    model = models.Atleta
    form_class = AtletaForm

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AtletaUpdate, self).get_context_data(**kwargs)
        # Add in context data
        context['titulo'] = "Actualizando un atleta"
        return context

class AtletasList(LoginRequiredMixin, ListView):
    model = models.Atleta
    queryset = Atleta.objects.all()
    template_name = 'atleta_list.html'
