from django.db import models
from django.urls import reverse


class Atleta(models.Model):
    dni = models.IntegerField(unique=True)
    nombre = models.CharField(max_length=140)
    apellido = models.CharField(max_length=140)
    fecha_nacimiento = models.DateField()
    domicilio = models.CharField(max_length=140)
    telefono_personal = models.CharField(max_length=40)
    telefono_referencia = models.CharField(max_length=40, null=True, blank=True)
    email = models.EmailField(max_length=240, null=True, blank=True)
    fecha_inicio_actividad = models.DateField(null=True, blank=True)
    observaciones = models.CharField(max_length=500, null=True, blank=True)
    foto = models.ImageField(upload_to='fotos_atletas/', null=True, blank=True)

    def __str__(self):
        return self.apellido + ', ' + self.nombre


    def get_absolute_url(self):
        return reverse('Entreno:atletas:detail', kwargs={'pk': self.id})

    class Meta:
        ordering = ["apellido", "nombre"]
        verbose_name = "Atleta"
        verbose_name_plural = "Atletas"


class Marca(models.Model):
    PRUEBA_CHOICES = (
        # pista
        (1, '60m'),
        (2, '100m llanos'),
        (3, '100m con vallas'),
        (4, '110m con vallas'),
        (5, '150m llanos'),
        (6, '200m llanos'),
        (7, '300m llanos'),
        (8, '400m llanos'),
        (9, '400m con vallas'),
        (10, '500m llanos'),
        (11, '600m llanos'),
        (12, '800m'),
        (14, '1000m'),
        (15, '1200m'),
        (16, '1500m'),
        (17, '3000m llanos'),
        (18, '3000m con obstáculos'),
        (19, '5000m'),
        (31, '10000m'),
        # de calle
        (120, '3000m calle'),
        (121, '5000m calle'),
        (122, '7000m calle'),
        (125, '10000m calle'),
        (127, '21095m calle'),
        (128, '42196m calle'),
        (129, '20000m marcha calle'),
        (130, '50000m marcha calle'),
        # cross
        (222, '5000m cross'),
        (223, '7000m cross'),
        (226, '10000m cross'),
        (227, '21095m cross'),
        (228, '42196m cross')
    )

    atleta = models.ForeignKey(Atleta, on_delete=models.CASCADE)
    fecha = models.DateField()
    descripcion = models.CharField(max_length=500, null=True, blank=True)
    prueba = models.IntegerField(choices=PRUEBA_CHOICES)
    marca = models.DurationField()

    class Meta:
        verbose_name = "Marca"
        verbose_name_plural = "Marcas"
