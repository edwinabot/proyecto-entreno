from django.urls import path, include
from . import views

urls_atletas = [
    path('add', views.AtletaCreate.as_view(), name='add'),
    path('<pk>/update', views.AtletaUpdate.as_view(), name='update'),
    path('list', views.AtletasList.as_view(), name='list'),
    path('<pk>/detail', views.AtletaDetail.as_view(), name='detail')
]

urlpatterns = [
    path('atletas/', include((urls_atletas, 'Entreno'), namespace='atletas'))
]
