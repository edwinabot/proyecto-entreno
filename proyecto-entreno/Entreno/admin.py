from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Atleta)
admin.site.register(models.Marca)
