from django import forms
from django.forms import ModelForm

from Entreno.models import Atleta
from PE.validators import no_future_date_validator


class AtletaForm(ModelForm):
    fecha_nacimiento = forms.DateField(validators=[no_future_date_validator], required=True)
    fecha_inicio_actividad = forms.DateField(validators=[no_future_date_validator], required=True)

    class Meta:
        model = Atleta
        fields = ['dni', 'nombre', 'apellido', 'fecha_nacimiento', 'domicilio', 'telefono_personal',
                  'telefono_referencia', 'email', 'fecha_inicio_actividad', 'observaciones', 'foto']
