from django import template
from django.forms import BoundField
from django.utils.safestring import SafeText

register = template.Library()


def classes(field: BoundField, extra_classes: SafeText):
    field.field.widget.attrs['class'] = extra_classes
    return field


register.filter(name='classes', filter_func=classes)
