from django.core.exceptions import ValidationError
from django.utils.timezone import now


def no_future_date_validator(value):
    if value > now().date():
        raise ValidationError(f'{value} no puede ser una fecha futura')
