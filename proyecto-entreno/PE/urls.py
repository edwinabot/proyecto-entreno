from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic import TemplateView

from PE.views import Home

urlpatterns = [
                  path('accounts/', include('django.contrib.auth.urls')),
                  path('admin/', admin.site.urls),
                  path('entreno/', include(('Entreno.urls', 'Entreno'))),
                  path('comercial/', include(('Comercial.urls', 'Comercial'))),
                  path('salud/', include(('Salud.urls', 'Salud'))),
                  path('', Home.as_view(), name='home')
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
