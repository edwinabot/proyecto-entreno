const ajaxify_forms = (function(window, document){
    let field_error_class = null;
    let field_error_message_class = null;
    const fields_with_error_class = [];
    const created_error_elements = [];

    return ajaxify_forms;

    function show_form_errors(response) {
        field_error_class = response.error_class;
        field_error_message_class = response.error_message_class;
        for (let field_id in response.errors) {
            let element = document.getElementById(field_id);
            fields_with_error_class.push(element.parentElement);
            element.parentElement.classList.add(field_error_class);

            response.errors[field_id].forEach((error) => {
                let error_div = document.createElement('div');
                error_div.classList.add(field_error_message_class);
                error_div.textContent = error;
                created_error_elements.push(error_div);
                element.parentElement.appendChild(error_div);
            });
        }
    }


    function clean_form_errors() {
        for (let error_element of created_error_elements) {
            error_element.remove();
        }
        for (let error_element of fields_with_error_class) {
            error_element.classList.remove(field_error_class)
        }
    }


    function on_submit(event) {
        event.preventDefault();
        clean_form_errors();
        let formData = new FormData(this);
        let csrftoken = formData.get("csrfmiddlewaretoken");
        let headers = new Headers();
        headers.append('X-CSRFToken', csrftoken);
        headers.append('X-Requested-With', 'XMLHttpRequest');
        fetch(this.action, {
            method: this.method,
            body: formData,
            headers: headers,
            credentials: 'include'
        })
        .then(response => response.json())
        .catch(console.error)
        .then(response => {
            if (response.hasOwnProperty('url')) {
                window.location.href = response.url;
            } else if (response.hasOwnProperty('errors')) {
                show_form_errors(response);
            }
        });
    }

    function ajaxify_forms(forms) {
        for (var i = 0; i < forms.length; i++) {
            forms[i].onsubmit = on_submit;
        }
    }
})(window, document);

document.addEventListener('DOMContentLoaded', () => {
    const forms = document.getElementsByClassName('ajax-form');
    ajaxify_forms(forms);
});