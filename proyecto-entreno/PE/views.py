import re

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views.generic import TemplateView

from Entreno.models import Atleta
from PE.settings import FIELD_ERROR_CLASS, FIELD_ERROR_MESSAGE_CLASS


def to_snake_case(name):
    s1 = re.sub(r'(\w+)(?:\W[ ]*)', r'\1', name)
    return re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class AjaxableResponseMixin:
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            data = {
                'errors': {form.auto_id % key: value for key, value in form.errors.items()},
                'error_class': FIELD_ERROR_CLASS,
                'error_message_class': FIELD_ERROR_MESSAGE_CLASS
            }
            return JsonResponse(data, status=400)
        else:
            return response

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        response = super().form_valid(form)
        if self.request.is_ajax():
            data = {
                'pk': self.object.pk,
                'url': self.get_success_url()
            }
            return JsonResponse(data)
        else:
            return response


class AjaxFormMixin:

    def get_form_class(self):
        class cls(super().get_form_class()):
            class Media:
                js = ['js/ajax-forms.js']

        return cls

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.classes = 'ajax-form'
        form.action = self.request.path
        return form


class Home(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Atleta.objects.all()
        return context
