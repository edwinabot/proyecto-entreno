import logging

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.timezone import now

from Entreno.models import Atleta


class FactorRiesgo(models.Model):
    detalle = models.CharField(blank=False, null=False, max_length=256)

    class Meta:
        ordering = ["detalle"]
        verbose_name = "Factor de riesgo"
        verbose_name_plural = "Factores de riesgo"

    def __str__(self):
        return self.detalle


class LegajoSalud(models.Model):
    FACTOR_SANGUINEO_CHOICES = (
        (1, 'O+'),
        (2, 'A+'),
        (3, 'B+'),
        (4, 'AB+'),
        (5, 'O-'),
        (6, 'A-'),
        (7, 'B-'),
        (8, 'AB-'),
    )

    atleta = models.OneToOneField(Atleta, on_delete=models.CASCADE)
    deslinde = models.BooleanField(default=False)
    factor_sanguineo = models.IntegerField(
        choices=FACTOR_SANGUINEO_CHOICES,
        null=True, blank=True)
    factores_riesgo = models.ManyToManyField(FactorRiesgo, blank=True)

    def get_absolute_url(self):
        return reverse('Salud:legajos:detail', kwargs={'pk': self.id})

    class Meta:
        ordering = ["atleta__apellido", "atleta__nombre"]
        verbose_name = "Legajo de Salud"
        verbose_name_plural = "Legajos de salud"

    def tiene_certificado_vigente(self):
        return self.aptofisico_set.filter(fecha_vencimiento__gt=now()).exists()

    def __str__(self):
        return 'Leg. salud: ' + self.atleta.apellido + ', ' + self.atleta.nombre


class AptoFisico(models.Model):
    legajo = models.ForeignKey(LegajoSalud, on_delete=models.CASCADE)
    fecha_vencimiento = models.DateField()
    fecha_presentado = models.DateField(default=now)

    class Meta:
        verbose_name = "Apto Físico"
        verbose_name_plural = "Aptos Físicos"

    def __str__(self):
        return 'Apto fisico: ' + str(self.legajo) + ', f. vto.: ' + str(self.fecha_vencimiento)


@receiver(post_save, sender=Atleta)
def __crear_legajo_salud_on_atleta_post_save(sender, **kwargs):
    try:
        atleta = kwargs.get('instance')
        legajo, created = LegajoSalud.objects.get_or_create(atleta=atleta)
        logging.info(f'Legajo de salud: {legajo} Creado: {created}')
    except Exception as e:
        logging.exception(e)
        raise e
