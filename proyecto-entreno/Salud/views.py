from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import ListView, UpdateView, CreateView, DetailView

from PE.views import AjaxableResponseMixin, AjaxFormMixin
from Salud.forms import LegajoSaludModelForm
from Salud.models import LegajoSalud
from . import models


class LegajoSaludDetail(LoginRequiredMixin, DetailView):
    model = models.LegajoSalud
    template_name = 'legajosalud_detail.html'

    def get_context_data(self, **kwargs):
        context = super(LegajoSaludDetail, self).get_context_data(**kwargs)
        context['titulo'] = f"Legajo de salud"
        return context


class LegajosTodosList(LoginRequiredMixin, ListView):
    model = models.LegajoSalud
    template_name = 'legajosalud_list.html'


class LegajoUpdate(LoginRequiredMixin, AjaxableResponseMixin, AjaxFormMixin, UpdateView):
    model = models.LegajoSalud
    template_name = 'legajosalud_form.html'
    form_class = LegajoSaludModelForm

    def get_context_data(self, **kwargs):
        context = super(LegajoUpdate, self).get_context_data(**kwargs)
        legajo = context['object']  # type: LegajoSalud
        context['titulo'] = f"Legajo de salud: {legajo.atleta}"
        return context


class AptoFisicoTodosList(LoginRequiredMixin, ListView):
    model = models.AptoFisico
    template_name = 'aptofisico_list.html'


class AptoFisicoUpdate(LoginRequiredMixin, UpdateView):
    model = models.AptoFisico
    template_name = 'aptofisico_form.html'
    fields = ['fecha_presentado', 'fecha_vencimiento']

    def get_legajo(self):
        return self.object.legajo

    def get_context_data(self, **kwargs):
        context = super(AptoFisicoUpdate, self).get_context_data(**kwargs)
        apto = context['object']  # type: models.AptoFisico
        context['titulo'] = f"Actualizar apto físico N°{apto.id}: {apto.legajo.atleta}"
        return context


class AptoFisicoCreate(LoginRequiredMixin, CreateView):
    model = models.AptoFisico
    template_name = 'aptofisico_form.html'
    fields = ['fecha_presentado', 'fecha_vencimiento']

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.legajo = None

    def get_legajo(self):
        if self.legajo is None:
            self.legajo = LegajoSalud.objects.get(pk=self.kwargs['legajo'])
        return self.legajo

    def get_initial(self):
        initial = super().get_initial()
        initial['legajo'] = self.get_legajo()
        return initial

    def form_valid(self, form):
        apto = form.save(commit=False)  # type:models.AptoFisico
        apto.legajo = self.get_legajo()
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AptoFisicoCreate, self).get_context_data(**kwargs)
        context['titulo'] = f"Nuevo apto físico: {self.get_legajo().atleta}"
        return context

    def get_success_url(self):
        return self.get_legajo().get_absolute_url()
