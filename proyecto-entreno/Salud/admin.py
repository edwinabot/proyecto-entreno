from django.contrib import admin

from . import models

# Register your models here.
admin.site.register(models.LegajoSalud)
admin.site.register(models.AptoFisico)
admin.site.register(models.FactorRiesgo)
