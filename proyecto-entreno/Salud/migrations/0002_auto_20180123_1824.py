# Generated by Django 2.0 on 2018-01-23 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Salud', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FactorRiesgo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('detalle', models.CharField(max_length=256)),
            ],
            options={
                'verbose_name': 'Factor de riesgo',
                'verbose_name_plural': 'Factores de riesgo',
                'ordering': ['detalle'],
            },
        ),
        migrations.RemoveField(
            model_name='legajosalud',
            name='factores_riesgo',
        ),
        migrations.AddField(
            model_name='legajosalud',
            name='factores_riesgo',
            field=models.ManyToManyField(blank=True, null=True, to='Salud.FactorRiesgo'),
        ),
    ]
