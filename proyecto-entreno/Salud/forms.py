from django.forms import ModelForm, ModelMultipleChoiceField

from Salud.models import FactorRiesgo
from .models import LegajoSalud


class LegajoSaludModelForm(ModelForm):
    factores_riesgo = ModelMultipleChoiceField(queryset=FactorRiesgo.objects.all(), required=False)

    class Meta:
        model = LegajoSalud
        exclude = ['atleta']

