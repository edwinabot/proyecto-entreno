from django.urls import path, include

from Salud import views

urls_aptos_fisicos = [
    path('list', views.LegajosTodosList.as_view(), name='list'),
    path('<pk>/update', views.AptoFisicoUpdate.as_view(), name='update'),
    path('<legajo>/create', views.AptoFisicoCreate.as_view(), name='create')
]

urls_legajos = [
    path('list', views.LegajosTodosList.as_view(), name='list'),
    path('<pk>/update', views.LegajoUpdate.as_view(), name='update'),
    path('<pk>/detail', views.LegajoSaludDetail.as_view(), name='detail')
]

urlpatterns = [
    path('legajos/', include((urls_legajos, 'Salud'), namespace='legajos')),
    path('aptos/fisicos/', include((urls_aptos_fisicos, 'Salud'), namespace='aptos_fisicos')),
]
