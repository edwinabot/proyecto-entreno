#!/bin/bash
. exports-production.sh
source venv/bin/activate
cd proyecto-entreno
gunicorn PE.wsgi:application --bind localhost:8034 --workers 3 --daemon --error-logfile errors.log --access-logfile access.log
deactivate
