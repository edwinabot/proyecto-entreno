#!/bin/bash
echo "Exporting variables..."
DB_PASSWORD="thepassword"
DB_USERNAME="theuser"
DB_NAME="thedatabase"
VERSION="v0.1"

export DB_PASSWORD
export DB_USERNAME
export DB_NAME
export VERSION