#!/bin/bash
. exports-production.sh
echo "Pulling changes..."
git checkout .
git pull --all
git checkout $VERSION
. database.sh
echo "Creating virtualenv..."
virtualenv --python=/usr/bin/python3 venv
source venv/bin/activate
python --version
echo "Installing requirements..."
cd proyecto-entreno
pip install -r requirements.txt
echo "Migrating database..."
python manage.py migrate -v 2 --noinput
echo "Collecting staticfiles..."
python manage.py collectstatic -v 2 --noinput
echo "Creating superuser..."
python manage.py createsuperuser -v 2 --username edwin --email edwinabot@gmail.com || true
deactivate
echo "Linking nginx conf..."
sudo ln -s $(pwd)/openmarathon /etc/nginx/sites-enabled/openmarathon
echo "Gunicorn HUP ..."
pkill -1 gunicorn
echo "Done ;)"