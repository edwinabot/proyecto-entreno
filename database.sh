#!/bin/bash
. exports-production.sh
sudo -u postgres psql -c "CREATE ROLE ${DB_USERNAME} WITH LOGIN ENCRYPTED PASSWORD '${DB_PASSWORD}'"
sudo -u postgres psql -c "CREATE DATABASE ${DB_NAME} OWNER ${DB_USERNAME}"
